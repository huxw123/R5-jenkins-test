class AddNotesIsPrivate < Rails.version < '5.1' ? ActiveRecord::Migration : ActiveRecord::Migration[4.2]
  def self.up
    add_column :notes, :is_private,:boolean,:default=>false, :null=>false

  end

  def self.down
    drop_table :notes,:is_private
  end
end