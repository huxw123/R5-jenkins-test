class CreateRemarks < ActiveRecord::Migration[6.1]
  def change
    create_table :remarks do |t|
      t.string :remarker
      t.text :body
      t.references :article, foreign_key: true

      t.timestamps
    end
  end
end
