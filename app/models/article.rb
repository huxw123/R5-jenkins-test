class Article < ActiveRecord::Base
  has_many :remarks
  validates :title,presence: true,length:{minimum:5}
end
