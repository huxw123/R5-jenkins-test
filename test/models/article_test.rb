require "test_helper"

class ArticleTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  test "should not save article without title" do
    article = Article.new
    assert_not article.save, "Saved the article without a title"
  end

  test "should report error" do
    # 测试用例中没有定义 some_undefined_variable  
    some_undefined_variable
    assert true
  end
end
