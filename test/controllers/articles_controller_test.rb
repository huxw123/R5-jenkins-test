require "test_helper"

class ArticlesControllerTest < ActionDispatch::IntegrationTest

  test "should create article" do
    assert_difference('Article.count') do
      post articles_url, params: { article: { title: 'Some title' } }
    end

    assert_redirected_to article_path(Article.last)
    assert_equal 'Article was successfully created.', flash[:notice]
  end
end
